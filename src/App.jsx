import { ChakraProvider } from '@chakra-ui/react';
import theme from './themes/todomejora';
import AppRouter from './navigations/Routes';
import { UserAuthContextProvider } from './context/UserAuthContext';

const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <UserAuthContextProvider>
        <AppRouter />
      </UserAuthContextProvider>
    </ChakraProvider>
  );
};

export default App;
