import { Box, Text } from '@chakra-ui/react';

const ChatList = () => {
  return (
    <Box>
      <Box
        w="22.563rem"
        h="4.125rem"
        borderRightWidth="0.125rem"
        borderBottomWidth="0.125rem"
        mt="-1.5rem"
      >
        <Text
          fontSize="1.25rem"
          fontStyle="normal"
          fontWeight="800"
          mt="1.438rem"
          ml="1.438rem"
        >
          Chats
        </Text>
      </Box>
      <Box
        w="22.563rem"
        h="43.75rem"
        borderRightWidth="0.125rem"
        overflowY="scroll"
      >
        <Text
          mt="17.625rem"
          ml="3.75rem"
          color="#8E8E8E"
        >
          No cuentas con chats activos
        </Text>
      </Box>
    </Box>
  );
};

export default ChatList;
