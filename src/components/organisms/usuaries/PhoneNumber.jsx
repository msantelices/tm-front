import { Container, Flex, Input, Text } from '@chakra-ui/react';
import { useState } from 'react';

import { useUserAuth } from '../../../context/UserAuthContext';
import { isNumeric } from '../../../utils/numberValidation';
import TMImage from '../../atoms/TMImage';
import TMButton from '../../atoms/TMButton';
import PhoneValidation from './PhoneValidation';

const PhoneNumber = () => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [validPhone, setValidPhone] = useState(false);
  const [pinSent, setPinSent] = useState(false);
  const [result, setResult] = useState('');

  const { setUpRecaptcha } = useUserAuth();

  const handlePhoneChange = (e) => {
    const inputValue = e.target.value;
    setPhoneNumber(inputValue);
    if (inputValue.length === 8 && isNumeric(inputValue)) {
      setValidPhone(true);
    } else {
      setValidPhone(false);
    }
  };

  const getOtpCode = async (e) => {
    e.preventDefault();
    try {
      const response = await setUpRecaptcha(phoneNumber);
      setResult(response);
      setPinSent(true);
    } catch (error) {
      console.error(error);
    }
  };

  const handleButtonColor = () => {
    if (validPhone) {
      return '#5C186A';
    } else {
      return '#8E8E8E';
    }
  };

  return (
    <Flex
      w="100%"
      direction="column"
      mt="1.875rem"
    >
      <Container centerContent>
        <TMImage
          src={'/images/logo-todo-mejora-icon.svg'}
          alt={'logo corazon todo mejora'}
          boxSize={'62px'}
        />
      </Container>
      <Container
        mt="1.75rem"
        centerContent
      >
        <Text
          color="#5C186A"
          lineHeight="2.125rem"
          fontSize="1.25rem"
          fontStyle="normal"
          fontWeight="600"
        >
          Ingresa tu número de teléfono
        </Text>
      </Container>
      <Container
        maxW="md"
        mt="1rem"
        centerContent
      >
        <Text
          color="#161616"
          lineHeight="1.25rem"
          fontSize="0.875rem"
          fontStyle="normal"
          fontWeight="400"
        >
          Te enviaremos un código a tu teléfono.
        </Text>
      </Container>
      <Container
        alignContent="center"
        centerContent
      >
        <Flex
          direction="row"
          mt="1.875rem"
          alignItems="center"
        >
          <TMImage
            src={'/images/smartphone.svg'}
            alt={'Ícono de Celular'}
            w={'1.25rem'}
          />
          <Text
            lineHeight="1.375rem"
            fontSize="1rem"
            fontStyle="normal"
            fontWeight="400"
            ml="0.3275rem"
            mr="0.3125rem"
          >
            +56 9
          </Text>
          <Input
            type="phone"
            borderColor="8E8E8E"
            w="15.125rem"
            placeholder=""
            value={phoneNumber}
            onChange={handlePhoneChange}
          />
        </Flex>
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="1.25rem"
        centerContent
      >
        <div id="recaptcha-container"></div>
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="1.25rem"
        centerContent
      >
        <TMButton
          textColor="#FFFFFF"
          w="20rem"
          h="2.875rem"
          bgColor={handleButtonColor}
          isDisabled={!validPhone || pinSent}
          onClick={getOtpCode}
        >
          Enviar código
        </TMButton>
      </Container>

      <Container style={{ display: pinSent ? 'block' : 'none' }}>
        <PhoneValidation result={result} />
      </Container>
    </Flex>
  );
};

export default PhoneNumber;
