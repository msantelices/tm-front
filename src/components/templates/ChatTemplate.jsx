import { Box, Center, Flex } from '@chakra-ui/react';

import TestProvider from '../../store/test/context';
import VoluntarieChatHeader from '../molecules/VoluntarieChatHeader';
import ChatList from '../organisms/voluntaries/ChatList';
import ChatView from '../organisms/voluntaries/ChatView';

const ChatTemplate = () => {
  return (
    <TestProvider>
      <Center>
        <Box
          w="89.938rem"
          h="53.063rem"
          borderWidth="0.125rem"
          mt="2.813rem"
        >
          <VoluntarieChatHeader />
          <Flex>
            <ChatList />
            <ChatView />
          </Flex>
        </Box>
      </Center>
    </TestProvider>
  );
};

export default ChatTemplate;
