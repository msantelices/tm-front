import {
  Box,
  Button,
  Center,
  Container,
  Flex,
  Input,
  Spacer,
  Text,
} from '@chakra-ui/react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from '../../firebaseConfig';
import TMImage from '../atoms/TMImage';

const AuthTemplate = () => {
  const [ready, setReady] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const emailValidator = /.+@.+\..+/;
  const navigate = useNavigate();

  const handleEmailInput = (e) =>
    emailValidator.test(e.target.value)
      ? setEmail(e.target.value)
      : setEmail('');

  const validateInputValues = (e) => {
    const password = e.target.value;

    if (password === '') setReady(false);
    if (email && password) {
      setPassword(password);
      setReady(true);
    }
  };

  const handleSubmit = (e) => {
    auth.signInWithEmailAndPassword(email, password).then((cred) => {
      navigate('/voluntaries/chat', { replace: true });
    });
  };

  return (
    <Flex
      direction="column"
      margin="146px"
    >
      <Container centerContent>
        <TMImage src={'/images/logo-todo-mejora.svg'} />
      </Container>
      <Box
        w="515px"
        h="600px"
        alignSelf="center"
        boxShadow="base"
        mt="40px"
      >
        <Center>
          <Flex
            id="volunteer-form"
            align="center"
            direction="column"
            w="425px"
            h="466px"
            mt="67px"
          >
            <Text
              fontSize="20px"
              fontWeight="600, Semi Bold"
              lineHeight="22px"
            >
              Inicia sesión con tu cuenta de Todo Mejora.
            </Text>
            <Spacer />
            <Box w="420px">
              <Text
                mb="10px"
                fontSize="16px"
                fontWeight="900"
                lineHeight="20px"
              >
                Mail de usuario
              </Text>
              <Input
                type="text"
                h="60px"
                fontSize="16px"
                fontWeight="400"
                lineHeight="22px"
                id="email"
                onBlur={handleEmailInput}
              />
            </Box>
            <Spacer />
            <Box w="420px">
              <Text
                mb="10px"
                fontSize="16px"
                fontWeight="900"
                lineHeight="20px"
              >
                Contraseña
              </Text>
              <Input
                type="password"
                fontSize="16px"
                alignContent="center"
                h="60px"
                id="password"
                onChange={validateInputValues}
                onBlur={validateInputValues}
              />
            </Box>
            <Spacer />
            {ready ? (
              <Button
                align="center"
                w="324px"
                bg="#161616"
                textColor="#FFFFFF"
                onClick={handleSubmit}
              >
                Entrar
              </Button>
            ) : (
              <Button
                align="center"
                w="324px"
                bg="#BEBEBE"
                textColor="#FFFFFF"
                onClick={handleSubmit}
              >
                Entrar
              </Button>
            )}
            <Spacer />
            <Text align="center">Olvidé mi contraseña</Text>
          </Flex>
        </Center>
      </Box>
    </Flex>
  );
};

export default AuthTemplate;
