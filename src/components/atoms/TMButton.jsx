import { Button } from '@chakra-ui/react';

const TMButton = (props) => {
  return (
    <Button
      fontStyle="normal"
      fontWeight="600"
      textTransform="uppercase"
      fontSize="0.875rem"
      {...props}
    ></Button>
  );
};

export default TMButton;
