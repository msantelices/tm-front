import { Button, Container, Flex, Spacer } from '@chakra-ui/react';

import TMImage from '../atoms/TMImage';

const UsuarieChatHeader = () => {
  return (
    <Container
      backgroundColor="#ffffff"
      boxShadow="2xl"
    >
      <Flex
        alignItems="center"
        justifyContent="center"
        h="5.188rem"
      >
        <TMImage
          src={'/images/logo-todo-mejora.svg'}
          alt={'Logo de Todo Mejora'}
        />
        <Spacer />
        <Button variant="ghost">
          <TMImage src={'/icons/options.svg'} />
        </Button>
      </Flex>
    </Container>
  );
};

export default UsuarieChatHeader;
