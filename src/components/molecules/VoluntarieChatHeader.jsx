import { Box, Flex, Spacer, Text } from '@chakra-ui/react';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import TMImage from '../atoms/TMImage';

const VoluntarieChatHeader = () => {
  const auth = firebase.auth();
  return (
    <Box
      w="89.938rem"
      h="5rem"
      borderBottomWidth="2px"
    >
      <Flex>
        <TMImage
          src={'/images/logo-todo-mejora.svg'}
          heartSize={'3.375rem'}
          mt={'0.875rem'}
          ml={'1.688rem'}
        />
        <Spacer />
        <Box
          mt="1.875rem"
          mr="2.875rem"
        >
          <Text>{auth.currentUser.email}</Text>
        </Box>
      </Flex>
    </Box>
  );
};

export default VoluntarieChatHeader;
