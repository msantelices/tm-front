import { Box, Flex, Text } from '@chakra-ui/react';
import '../../App.css';

const ReceivedMessage = () => {
  return (
    <Flex
      alignItems="start"
      direction="column"
      maxW="md"
    >
      <Box
        mt="0.75rem"
        backgroundColor="#ffffff"
        borderRadius="0.562rem"
        alignItems="end"
        className={'receivedMessage'}
      >
        <Text
          color="#333333"
          fontSize="0.875rem"
          fontWeight="400"
          m="1rem"
        >
          ¡Hola! Te damos la bienvenida a este espacio seguro para conversar.
          Puedes revisar nuestros términos y condiciones y nuestra política de
          privacidad en el menú superior
        </Text>
      </Box>
    </Flex>
  );
};

export default ReceivedMessage;
