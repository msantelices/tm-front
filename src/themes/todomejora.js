import { extendTheme } from '@chakra-ui/react';

const theme = extendTheme({
  fonts: {
    body: 'noto-sans, sans-serif',
  },
  textStyles: {
    h2: {
      fontFamily: 'noto-sans, sans-serif',
      fontSize: '20px',
      fontWeight: 'semibold',
      lineHeight: '22px',
    },
    h3: {
      fontFamily: 'noto-sans, sans-serif',
      fontSize: '20px',
      fontWeight: '800',
      lineHeight: '22px',
    },
  },
});

export const buttonProps = {
  h: '3.125rem',
  color: '#8E8E8E',
  lineHeight: '1.125rem',
  fontSize: '0.875rem',
  fontStyle: 'normal',
  fontWeight: '400',
  border: '1px',
  borderColor: '#8E8E8E',
  opacity: '0.8',
};

export const buttonPropsClicked = {
  h: '3.125rem',
  backgroundColor: '#5C186A',
  lineHeight: '1.125rem',
  fontSize: '0.875rem',
  fontStyle: 'normal',
  fontWeight: '400',
  border: '1px',
  borderColor: '#A5A5A5',
  color: '#FFFFFF',
};

export default theme;
