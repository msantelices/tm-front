import { Container, Flex } from '@chakra-ui/react';

import UsuarieChatHeader from '../../components/molecules/UsuarieChatHeader';
import ReceivedMessage from '../../components/molecules/ReceivedMessage';
import SentMessage from '../../components/molecules/SentMessage';
import UsuarieChatBottom from '../../components/molecules/UsuarieChatBottom';

const UsuarieChat = () => {
  return (
    <Flex
      w="100%"
      direction="column"
      mt="1.875rem"
    >
      <UsuarieChatHeader />
      <Container
        h="45rem"
        backgroundColor="#e5e5e5"
        pt="2rem"
        boxShadow="2xl"
      >
        <ReceivedMessage />
        <SentMessage />
      </Container>
      <UsuarieChatBottom />
    </Flex>
  );
};

export default UsuarieChat;
