import { Container, Flex, Text } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

import TMImage from '../../components/atoms/TMImage';
import TMLinkButton from '../../components/atoms/TMLinkButton';
import TMButton from '../../components/atoms/TMButton';

const UsuarieWelcome = () => {
  const navigate = useNavigate();
  const handleClick = () => navigate('/usuaries/auth');

  return (
    <Flex
      w="100%"
      direction="column"
      mt="1.875rem"
      alignItems="center"
    >
      <Container centerContent>
        <TMImage
          src={'/images/logo-todo-mejora-icon.svg'}
          alt={'logo corazon todo mejora'}
          boxSize={'62px'}
        />
      </Container>
      <Container
        maxW="md"
        mt="1.75rem"
        centerContent
      >
        <Text
          color="#7C4188"
          lineHeight="2.125rem"
          fontSize="1.5rem"
          fontStyle="normal"
          fontWeight="700"
        >
          ¿Quieres Conversar?
        </Text>
        <Text
          color="#7C4188"
          lineHeight="2.125rem"
          fontSize="1.375rem"
          fontStyle="normal"
          fontWeight="400"
        >
          ¿Necesitas Contención?
        </Text>
      </Container>
      <Container centerContent>
        <TMImage
          src={'/images/heart-support.svg'}
          alt={'personas cuidando un corazón'}
          w={'13.75rem'}
          h={'9.5625rem'}
          mt={'0.4375rem'}
        />
      </Container>
      <Container
        maxW="md"
        mt="1rem"
        centerContent
      >
        <Text
          color="#161616"
          lineHeight="1.375rem"
          fontSize="1rem"
          fontStyle="normal"
          fontWeight="400"
          width="19.5rem"
        >
          En la Hora Segura entregamos contención y orientación{' '}
          <span style={{ fontWeight: 'bold' }}>confidencial y gratuita</span> a
          niñas, niños, niñes, adolescentes y jóvenes LGBTIQA+ y a su entorno
          cercano.
        </Text>
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="1.25rem"
      >
        <TMButton
          bgColor="#5C186A"
          textColor="#FFFFFF"
          onClick={handleClick}
        >
          Necesito contención y orientación
        </TMButton>
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="0.75rem"
      >
        <TMButton
          bgColor="#FFFFFF"
          textColor="#5C186A"
          border="0.0625rem solid #5C186A"
          onClick={handleClick}
        >
          Quiero preguntar por alguien más
        </TMButton>
      </Container>
      <Container
        maxW="md"
        mt="1.75rem"
        centerContent
      >
        <TMLinkButton href="https://www.google.com">
          Términos y condiciones
        </TMLinkButton>
      </Container>
    </Flex>
  );
};

export default UsuarieWelcome;
